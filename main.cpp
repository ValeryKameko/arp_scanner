#define WINVER 0x0600
#define _WIN32_WINNT 0x0600

#include <winsock2.h>
#include <WS2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <map>
#include <ios>
#include <future>
#include <mutex>
#include <queue>
#include <cmath>

const ULONG WORKING_BUFFER_SIZE = 15000;

std::wstring InterfaceTypeToString(IFTYPE interfaceType) {
	const static std::map<IFTYPE, const std::wstring> interfaceDescriptions = {
		{ IF_TYPE_ETHERNET_CSMACD, L"Ethernet" },
		{ IF_TYPE_ISO88025_TOKENRING, L"Token Ring" },
		{ IF_TYPE_PPP, L"PPP" },
		{ IF_TYPE_SOFTWARE_LOOPBACK, L"Software Loopback" },
		{ IF_TYPE_ATM, L"ATM" },
		{ IF_TYPE_IEEE80211, L"IEEE 802.11" },
		{ IF_TYPE_TUNNEL, L"Tunnel" },
		{ IF_TYPE_IEEE1394, L"IEEE 1394"}
	};
	const static std::wstring unknownInterfaceName = L"Unknown";

	auto interfaceDescription = interfaceDescriptions.find(interfaceType);
	if (interfaceDescription != interfaceDescriptions.cend())
		return interfaceDescription->second;
	else
		return unknownInterfaceName;
}

std::wstring InterfaceStatusToString(IF_OPER_STATUS interfaceStatus) {
	const static std::map<IF_OPER_STATUS, const std::wstring> interfaceStatusDescriptions = {
		{ IfOperStatusUp, L"Up" },
		{ IfOperStatusDown, L"Down" },
		{ IfOperStatusTesting, L"Testing" },
		{ IfOperStatusUnknown, L"Unknown" },
		{ IfOperStatusDormant, L"Dormant" },
		{ IfOperStatusNotPresent, L"Not Present" },
		{ IfOperStatusLowerLayerDown, L"Lower Layer Down" }
	};
	const static std::wstring unknownInterfaceStatusString = L"Unknown";

	auto interfaceStatusDescription = interfaceStatusDescriptions.find(interfaceStatus);
	if (interfaceStatusDescription != interfaceStatusDescriptions.cend())
		return interfaceStatusDescription->second;
	else
		return unknownInterfaceStatusString;
}

std::vector<std::pair<std::wstring, std::wstring> > InterfaceFlagsToVector(PIP_ADAPTER_ADDRESSES adapterAddresses) {
	const static std::wstring statusDescriptions[2] = { L"Disabled", L"Enabled" };
	std::vector<std::pair<std::wstring, ULONG> > flagsPairs = {
		{ L"DdnsEnabled", adapterAddresses->DdnsEnabled },
		{ L"RegisterAdapterSuffix", adapterAddresses->RegisterAdapterSuffix },
		{ L"Dhcpv4Enabled", adapterAddresses->Dhcpv4Enabled },
		{ L"ReceiveOnly", adapterAddresses->ReceiveOnly },
		{ L"NoMulticast", adapterAddresses->NoMulticast },
		{ L"Ipv6OtherStatefulConfig", adapterAddresses->Ipv6OtherStatefulConfig },
		{ L"NetbiosOverTcpipEnabled", adapterAddresses->NetbiosOverTcpipEnabled },
		{ L"Ipv4Enabled", adapterAddresses->Ipv4Enabled },
		{ L"Ipv6Enabled", adapterAddresses->Ipv6Enabled },
		{ L"Ipv6ManagedAddressConfigurationSupported", adapterAddresses->Ipv6ManagedAddressConfigurationSupported }
	};
	std::vector<std::pair<std::wstring, std::wstring> > resultPairs;
	for (const auto &flagsPair : flagsPairs)
		resultPairs.push_back({flagsPair.first, statusDescriptions[flagsPair.second]});
	return resultPairs;
}

std::wstring AddressToString(LPSOCKADDR sockaddr) {
	if (!sockaddr)
		return L"";
	constexpr size_t localBufferSize = 256;
	static wchar_t localBuffer[localBufferSize];
	std::wstringstream addressStringStream;
	if (sockaddr->sa_family == AF_INET) {
		sockaddr_in *sa_in = (sockaddr_in *)sockaddr;
		addressStringStream << InetNtopW(AF_INET, &(sa_in->sin_addr), localBuffer, localBufferSize);
	}
	else if (sockaddr->sa_family == AF_INET6) {
		sockaddr_in6 *sa_in = (sockaddr_in6 *)sockaddr;
		addressStringStream << InetNtopW(AF_INET6, &(sa_in->sin6_addr), localBuffer, localBufferSize);
	}
	else {
		addressStringStream << "Unknown";
	}

	return addressStringStream.str();
}

std::vector<std::wstring> UnicastAddressesToVector(PIP_ADAPTER_UNICAST_ADDRESS adapterUnicastAddressList) {
	std::vector<std::wstring> resultVector;
	constexpr size_t localBufferSize = 256;
	static wchar_t localBuffer[localBufferSize];
	for (PIP_ADAPTER_UNICAST_ADDRESS currentUnicastAddress = adapterUnicastAddressList;
		currentUnicastAddress;
		currentUnicastAddress = currentUnicastAddress->Next)
	{
		std::wstring unicastAddressString = L"";
		unicastAddressString = AddressToString(currentUnicastAddress->Address.lpSockaddr);
		unicastAddressString += L" with subnet mask " + std::to_wstring(currentUnicastAddress->OnLinkPrefixLength);
		resultVector.push_back(unicastAddressString);

	}
	return resultVector;
}

std::vector<std::wstring> AnycastAddressesToVector(PIP_ADAPTER_ANYCAST_ADDRESS adapterAnycastAddressList) {
	std::vector<std::wstring> resultVector;
	for (PIP_ADAPTER_ANYCAST_ADDRESS currentAnycastAddress = adapterAnycastAddressList;
		currentAnycastAddress;
		currentAnycastAddress = currentAnycastAddress->Next)
	{
		resultVector.push_back(AddressToString(currentAnycastAddress->Address.lpSockaddr));
	}
	return resultVector;
}

std::vector<std::wstring> MulticastAddressesToVector(PIP_ADAPTER_MULTICAST_ADDRESS adapterMulticastAddressList) {
	std::vector<std::wstring> resultVector;
	for (PIP_ADAPTER_MULTICAST_ADDRESS currentMulticastAddress = adapterMulticastAddressList;
		currentMulticastAddress;
		currentMulticastAddress = currentMulticastAddress->Next)
	{
		resultVector.push_back(AddressToString(currentMulticastAddress->Address.lpSockaddr));
	}
	return resultVector;
}

std::vector<std::wstring> DNSServerAddressesToVector(PIP_ADAPTER_DNS_SERVER_ADDRESS adapterDNSServerAddressList) {
	std::vector<std::wstring> resultVector;
	for (PIP_ADAPTER_DNS_SERVER_ADDRESS currentDNSServerAddress = adapterDNSServerAddressList;
		currentDNSServerAddress;
		currentDNSServerAddress = currentDNSServerAddress->Next)
	{
		resultVector.push_back(AddressToString(currentDNSServerAddress->Address.lpSockaddr));
	}
	return resultVector;
}

std::vector<std::wstring> InterfacePrefixesToVector(PIP_ADAPTER_PREFIX adapterIPPrefixList) {
	std::vector<std::wstring> resultIPPrefixes;
	for (PIP_ADAPTER_PREFIX currentIPPrefix = adapterIPPrefixList;
		currentIPPrefix;
		currentIPPrefix = currentIPPrefix->Next) 
	{
		resultIPPrefixes.push_back(AddressToString(currentIPPrefix->Address.lpSockaddr) + L"/" + std::to_wstring(currentIPPrefix->PrefixLength));
 	}

	return resultIPPrefixes;
}

std::vector<std::wstring> InterfaceGatewayAddressesToVector(PIP_ADAPTER_GATEWAY_ADDRESS adapterGatewayAddressesList) {
	std::vector<std::wstring> resultVector;
	for (PIP_ADAPTER_GATEWAY_ADDRESS currentGatewayAddress = adapterGatewayAddressesList;
		currentGatewayAddress;
		currentGatewayAddress = currentGatewayAddress->Next) 
	{
		resultVector.push_back(AddressToString(currentGatewayAddress->Address.lpSockaddr));
	}
	return resultVector;
}

std::wstring SpeedToString(ULONG64 speed) {
	const static std::vector<std::wstring> prefixesUnit = { L"", L"k", L"M", L"G", L"T", L"P" };
	const static std::wstring measureUnitString = L"bit/s";
	
	long double speedUnit = speed;
	UINT currentPrefix = 0;
	while (speedUnit > 1000 && currentPrefix + 1 < prefixesUnit.size()) {
		speedUnit /= 1000;
		++currentPrefix;
	}
	std::wstringstream resultStringStream;
	resultStringStream << std::fixed << std::setprecision(1) << speedUnit << " " << prefixesUnit[currentPrefix] << measureUnitString;
	return resultStringStream.str();
}

std::wstring ConnectionTypeToString(NET_IF_CONNECTION_TYPE connectionType) {
	std::wstring resultString;
	switch (connectionType) {
	case NET_IF_CONNECTION_DEDICATED:
		resultString = L"DEDICATED";
		break;
	case NET_IF_CONNECTION_PASSIVE:
		resultString = L"PASSIVE";
		break;
	case NET_IF_CONNECTION_DEMAND:
		resultString = L"DEMAND";
		break;
	case NET_IF_CONNECTION_MAXIMUM:
		resultString = L"MAXIMUM";
		break;
	}
	return resultString;
}

std::wstring TunnelTypeToString(TUNNEL_TYPE tunnelType) {
	std::wstring resultString;
	switch (tunnelType) {
	case TUNNEL_TYPE_NONE:
		resultString = L"None";
		break;
	case TUNNEL_TYPE_OTHER:
		resultString = L"Other";
		break;
	case TUNNEL_TYPE_DIRECT:
		resultString = L"Direct";
		break;
	case TUNNEL_TYPE_6TO4:
		resultString = L"IPv4 -> IPv6";
		break;
	case TUNNEL_TYPE_ISATAP:
		resultString = L"ISATAP";
		break;
	case TUNNEL_TYPE_TEREDO:
		resultString = L"TEREDO";
		break;
	case TUNNEL_TYPE_IPHTTPS:
		resultString = L"IP HTTPS";
		break;
	}
	return resultString;
}

PIP_ADAPTER_UNICAST_ADDRESS FindIPv4UnicastAddress(PIP_ADAPTER_UNICAST_ADDRESS unicastAddressesList) {
	for (PIP_ADAPTER_UNICAST_ADDRESS currentUnicastAddress = unicastAddressesList;
		currentUnicastAddress;
		currentUnicastAddress = currentUnicastAddress->Next) 
	{
		if (currentUnicastAddress->Address.lpSockaddr->sa_family == AF_INET)
			return currentUnicastAddress;
	}
	return nullptr;
}

bool IsInsideSubnet(ULONG subnetAddress, ULONG subnetMask, ULONG hostAddress) {
	ULONG hostIndex = ntohl(hostAddress) & ntohl((~subnetMask));
	return (ntohl(subnetMask) & ntohl(hostAddress)) == (ntohl(subnetAddress) & ntohl(hostAddress)) &&
		hostIndex != ntohl(~subnetMask) &&
		hostIndex != 0;
}

struct SubnetHostInformation {
	IPAddr IP;
	std::vector<BYTE> MACAddress;
	std::wstring Name;

	SubnetHostInformation(IPAddr ip, const std::vector<BYTE> &macAddress, const std::wstring &name) 
		: IP(ip), MACAddress(macAddress), Name(name)
	{ }
};

std::vector<BYTE> MACAddressToVector(PBYTE macAddress, ULONG macAddressLength) {
	std::vector<BYTE> macAddressVector;
	for (ULONG i = 0; i < macAddressLength; i++)
		macAddressVector.push_back(macAddress[i]);
	return macAddressVector;
}

std::wstring MACAddressToString(std::vector<BYTE> macAddress) {
	std::wstringstream MACStringStream;
	for (size_t i = 0; i < macAddress.size(); i++) {
		if (i != 0)
			MACStringStream << ":";
		MACStringStream << std::setbase(16) << std::setfill(L'0') << std::setw(2) << macAddress[i];
	}
	return MACStringStream.str();
}

std::vector<SubnetHostInformation> FindSubnetHosts(PIP_ADAPTER_ADDRESSES adapterAddresses) {
	std::vector<SubnetHostInformation> hosts;
	if (adapterAddresses->OperStatus != IfOperStatusUp || adapterAddresses->IfType == IF_TYPE_SOFTWARE_LOOPBACK)
		return hosts;
	PIP_ADAPTER_UNICAST_ADDRESS myUnicastIPv4 = FindIPv4UnicastAddress(adapterAddresses->FirstUnicastAddress);
	IN_ADDR address = ((sockaddr_in*)myUnicastIPv4->Address.lpSockaddr)->sin_addr;
	UINT8 subnetMaskPrefix = myUnicastIPv4->OnLinkPrefixLength;
	IPAddr subnetMask;
	ConvertLengthToIpv4Mask(subnetMaskPrefix, &subnetMask);
	
	IPAddr subnetAddress = subnetMask & address.s_addr;

	IPAddr firstHostAddress = ntohl(address.s_addr & subnetMask);
	IPAddr lastHostAddress = ntohl(address.s_addr | (~subnetMask));

	int threadPoolSize = min(1000, max(1, lastHostAddress - firstHostAddress + 1));
	std::vector<std::thread> threadPool;
	std::queue<IPAddr> taskAddressPool;

	std::atomic<bool> stopThreads;
	std::condition_variable newTask, tasksEmpty;
	std::mutex useTaskAddressPool;
	std::mutex useIO;
	std::mutex useHostVector;
	bool stopWorking = false;
	int gnt = 0;

	for (int i = 0; i < threadPoolSize; i++) {
		threadPool.emplace_back([&]() {
			constexpr int localBufferSize = 256;
			BYTE localBuffer[localBufferSize];
			IPAddr currentAddress;
			while (true) {
				{
					std::unique_lock<std::mutex> lock(useTaskAddressPool);
					newTask.wait(lock, [&]() { return stopWorking || !taskAddressPool.empty(); });
					if (stopWorking)
						return;
					currentAddress = taskAddressPool.front();
					taskAddressPool.pop();
					if (taskAddressPool.size() == 0)
						tasksEmpty.notify_one();
				}
				IPAddr srcIP = address.s_addr;
				IPAddr destIP = currentAddress;
				ULONG macAddressLength = localBufferSize;
				DWORD result = SendARP(destIP, srcIP, localBuffer, &macAddressLength);
				if (result == NO_ERROR) {
					std::vector<BYTE> macAddress = MACAddressToVector(localBuffer, macAddressLength);
					constexpr size_t nameLength = 256;
					WCHAR hostName[nameLength], servName[nameLength];

					sockaddr_in hostAddress;
					hostAddress.sin_family = AF_INET;
					hostAddress.sin_addr.s_addr = destIP;
					hostAddress.sin_port = 0;

					DWORD returnValue = GetNameInfoW((SOCKADDR*)&hostAddress, 
						sizeof(hostAddress), 
						hostName, nameLength, servName, nameLength, NI_NUMERICSERV);
					std::wstring hostNameString = (returnValue == 0) ? std::wstring(hostName) : std::wstring(L"Unknown");
					{
						std::unique_lock<std::mutex> lock(useHostVector);
						hosts.push_back(SubnetHostInformation(destIP, macAddress, hostNameString));
					}
				}
			}
		});
	}

	constexpr int localBufferSize = 256;
	BYTE localBuffer[localBufferSize];
	int cnt = 0;

	for (IPAddr currentHostAddress = firstHostAddress;
		currentHostAddress <= lastHostAddress;
		currentHostAddress++)
	{
		IPAddr currentAddress = htonl(currentHostAddress);
		{
			std::unique_lock<std::mutex> lock(useTaskAddressPool);
			taskAddressPool.push(currentAddress);
			if (taskAddressPool.size() > 0)
				newTask.notify_all();
		}
	}

	{
		std::unique_lock<std::mutex> lock(useTaskAddressPool);
		tasksEmpty.wait(lock, [&] () { return taskAddressPool.empty(); });
	}

	stopWorking = true;
	newTask.notify_all();
		
	for (auto &thrd : threadPool) {
		thrd.join();
	}

	return hosts;
}

void PrintAdapterInfo(PIP_ADAPTER_ADDRESSES adapterAddresses) {
	constexpr ULONG localBufferSize = 256;
	static wchar_t localBuffer[localBufferSize];

	std::wcout << L"Adapter:\n";
	std::wcout << L"\tName - " << adapterAddresses->AdapterName << "\n";

	std::wcout << L"\tUnicast IPs:\n";
	for (const auto &ipAddress : UnicastAddressesToVector(adapterAddresses->FirstUnicastAddress))
		std::wcout << "\t\t" << ipAddress << "\n";

	std::wcout << L"\tAnycast IPs:\n";
	for (const auto &ipAddress : AnycastAddressesToVector(adapterAddresses->FirstAnycastAddress))
		std::wcout << "\t\t" << ipAddress << "\n";

	std::wcout << "\tMulticast IPs:\n";
	for (const auto &ipAddress : MulticastAddressesToVector(adapterAddresses->FirstMulticastAddress))
		std::wcout << "\t\t" << ipAddress << "\n";

	std::wcout << "\tDNS Server IPs:\n";
	for (const auto &ipAddress : DNSServerAddressesToVector(adapterAddresses->FirstDnsServerAddress))
		std::wcout << "\t\t" << ipAddress << "\n";

	std::wcout << "\tDNS Suffix - " << adapterAddresses->DnsSuffix << "\n";
	std::wcout << "\tDescription - " << adapterAddresses->Description << "\n";
	std::wcout << "\tFriendly Name - " << adapterAddresses->FriendlyName << "\n";
	std::wcout << "\tMAC - " << MACAddressToString(MACAddressToVector(adapterAddresses->PhysicalAddress, adapterAddresses->PhysicalAddressLength)) << "\n";

	std::wcout << "\tFlags:\n";
	for (const auto &flagsPair : InterfaceFlagsToVector(adapterAddresses))
		std::wcout << "\t\t" << std::left << std::setw(50) << flagsPair.first << " = " << flagsPair.second << "\n";
	
	std::wcout << "\tMTU - " << adapterAddresses->Mtu << " bits\n";
	std::wcout << "\tInterface Type - " << InterfaceTypeToString(adapterAddresses->IfType) << "\n";
	std::wcout << "\tOperational Status - " << InterfaceStatusToString(adapterAddresses->OperStatus) << "\n";

	std::wcout << "\tIP prefixes:\n";
	for (const auto &ipAddressPrefix : InterfacePrefixesToVector(adapterAddresses->FirstPrefix))
		std::wcout << "\t\t" << ipAddressPrefix << "\n";

	std::wcout << "\tTransmit link speed - " << SpeedToString(adapterAddresses->TransmitLinkSpeed) << "\n";
	std::wcout << "\tReceive link speed - " << SpeedToString(adapterAddresses->ReceiveLinkSpeed) << "\n";

	std::wcout << "\tGateway addresses:\n";
	for (const auto &gatewayAddressPrefix : InterfaceGatewayAddressesToVector(adapterAddresses->FirstGatewayAddress))
		std::wcout << "\t\t" << gatewayAddressPrefix << "\n";

	std::wcout << "\tIPv4 Metrics - " << adapterAddresses->Ipv4Metric << "\n";
	std::wcout << "\tIPv6 Metrics - " << adapterAddresses->Ipv6Metric << "\n";
	std::wcout << "\tLUID - " << adapterAddresses->Luid.Info.IfType << "-" << adapterAddresses->Luid.Info.NetLuidIndex << "\n";
	std::wcout << "\tDHCP IPv4 Server - " << AddressToString(adapterAddresses->Dhcpv4Server.lpSockaddr) << "\n";
	std::wcout << "\tDHCP IPv6 Server - " << AddressToString(adapterAddresses->Dhcpv6Server.lpSockaddr) << "\n";
	std::wcout << "\tCompartment Id - " << adapterAddresses->CompartmentId << "\n";
	std::wcout << "\tNetwork GUID - " << 
		adapterAddresses->NetworkGuid.Data1 << "-" <<
		adapterAddresses->NetworkGuid.Data2 << "-" <<
		adapterAddresses->NetworkGuid.Data3 << "-" <<
		adapterAddresses->NetworkGuid.Data4 << "\n";
	std::wcout << "\tConnection Type - " << ConnectionTypeToString(adapterAddresses->ConnectionType) << "\n";
	std::wcout << "\tTunnel Type - " << TunnelTypeToString(adapterAddresses->TunnelType) << "\n";
	std::wcout << "\n";

	std::wcout << "\tHosts:\n";
	for (const auto &host : FindSubnetHosts(adapterAddresses)) {
		std::wcout << "\t\tIP - " << std::setw(20) << InetNtopW(AF_INET, &host.IP, localBuffer, localBufferSize) << ", MAC - " << std::setw(20) << MACAddressToString(host.MACAddress) << ", Name - " << std::setw(20) << host.Name << "\n";
	}
	std::wcout << "\n\n\n";
}

int wmain() {
	std::ios_base::sync_with_stdio(false);
	setlocale(LC_ALL, "russian");

	SetConsoleCP(CP_UTF8);
	SetConsoleOutputCP(CP_UTF8);

	// Initialize Winsock
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		wprintf(L"WSAStartup failed: %d\n", iResult);
		return 1;
	}

	ULONG family = AF_UNSPEC;
	ULONG flags = GAA_FLAG_INCLUDE_PREFIX | GAA_FLAG_INCLUDE_GATEWAYS;
	PIP_ADAPTER_ADDRESSES adapterAddressesList = nullptr;
	ULONG bufferSize = WORKING_BUFFER_SIZE;

	adapterAddressesList = (PIP_ADAPTER_ADDRESSES)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, bufferSize);
	ULONG getAdaptersResult = GetAdaptersAddresses(family, flags, NULL, adapterAddressesList, &bufferSize);
	if (getAdaptersResult == NO_ERROR) {
		for (PIP_ADAPTER_ADDRESSES currentAdapterAddress = adapterAddressesList;
			currentAdapterAddress;
			currentAdapterAddress = currentAdapterAddress->Next) 
		{
			PrintAdapterInfo(currentAdapterAddress);
		}
	}
	HeapFree(GetProcessHeap(), 0, adapterAddressesList);
	system("pause");
	return 0;
}